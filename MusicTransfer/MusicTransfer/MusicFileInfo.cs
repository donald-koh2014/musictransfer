﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using MusicTransfer.MusicFiles;

namespace MusicTransfer {
    public struct MusicFileInfo {
        public string AlbumArtist;
        public string Album;
        public int DiscNumber;
        public int TrackNumber;
        public string Title;
        public MusicFileType MusicFileType;
        public long CopiedCount;
        public DateTime LastCopiedDateTime;
        public string Path;
        public long FileSize;
        public bool IsExclusion;

        public MusicFileInfo(string readedLine) {
            var words = readedLine.Split('\t');

            if ( readedLine.Length < 10 ) {
                throw new ArgumentException("項目数が不足しています。TAB区切りで10項目必要です。", readedLine);
            }

            AlbumArtist = words[0];
            Album = words[1];
            if ( !Int32.TryParse(words[2], out DiscNumber) ) {
                DiscNumber = -1;
            }
            if ( !Int32.TryParse(words[3], out TrackNumber) ) {
                TrackNumber = -1;
            }
            Title = words[4];

            MusicFileType = words[5] switch
            {
                "Flac" => MusicFileType.Flac,
                "M4a" => MusicFileType.M4a,
                _ => MusicFileType.Unknown
            };

            if ( !Int64.TryParse(words[6], out CopiedCount) ) {
                CopiedCount = 0;
            }

            if ( !DateTime.TryParse(words[7], out LastCopiedDateTime) ) {
                LastCopiedDateTime = DateTime.MinValue;
            }

            Path = words[8];
            if ( !Int64.TryParse(words[9], out FileSize) ) {
                FileSize = -1;
            }

            if ( 10 < words.Length ) {
                IsExclusion = Boolean.Parse(words[10]);
            } else {
                IsExclusion = false;
            }
        }

        public MusicFileInfo(MusicFile musicFile, string path, long fileSize) {
            AlbumArtist = musicFile.GetTag("ALBUMARTIST");
            Album = musicFile.GetTag("ALBUM");
            Int32.TryParse(musicFile.GetTag("DISCNUMBER"), out DiscNumber);
            Int32.TryParse(musicFile.GetTag("TRACKNUMBER"), out TrackNumber);
            Title = musicFile.GetTag("TITLE");
            if ( musicFile is Flac ) {
                MusicFileType = MusicFileType.Flac;
            } else if ( musicFile is M4a ) {
                MusicFileType = MusicFileType.M4a;
            } else {
                MusicFileType = MusicFileType.Unknown;
            }

            CopiedCount = 0;
            LastCopiedDateTime = DateTime.MinValue;
            Path = path;
            FileSize = fileSize;
            IsExclusion = false;
        }

        public override string ToString() {
            return String.Join("\t", AlbumArtist, Album, DiscNumber, TrackNumber, Title, MusicFileType.ToString(), CopiedCount, LastCopiedDateTime.ToString("yyyy/MM/dd HH:mm:ss"), Path, FileSize, IsExclusion);
        }

        public static void WriteTo(Stream stream, IList<MusicFileInfo> fileList) {
            using var writer = new StreamWriter(stream, Encoding.UTF8);

            writer.WriteLine("AlbumArtist\tAlbum\tDiscNumber\tTrackNumber\tTitle\tMusicFileType\tCopiedCount\tLastCopiedDateTime\tPath\tFileSize\tExclusionFlag");
            foreach ( var mfi in fileList ) {
                writer.WriteLine(mfi.ToString());
            }
        }

        public static MusicFileInfo[] ReadFrom(StreamReader reader) {
            var fileList = new List<MusicFileInfo>();

            reader.ReadLine();  // 1行目この行はヘッダなので読み飛ばす
            while ( !reader.EndOfStream ) {
                fileList.Add(new MusicFileInfo(reader.ReadLine()));
            }

            return fileList.ToArray();
        }
    }
}
