﻿namespace MusicTransfer {
    public struct AlbumInfo {
        public string Album { get; private set; }
        public double CopiedCount { get; private set; }
        public AlbumInfo(string album, double copiedCount) {
            Album = album;
            CopiedCount = copiedCount;
        }
    }
}
