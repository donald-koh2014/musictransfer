﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MusicTransfer.Tools {
    public static class Tool {
        public static byte[] ChangeEndian(byte[] bytes) {
            var bs = new byte[bytes.Length];

            for ( var i = 0; i < bytes.Length; i++ ) {
                bs[bytes.Length - 1 - i] = bytes[i];
            }

            return bs;
        }

        private static IEnumerable<string> SelectDirectories(string title, string initialDirectory, bool multiSelect) {
            using var d = new Microsoft.WindowsAPICodePack.Dialogs.CommonOpenFileDialog {
                Title = title,
                InitialDirectory = initialDirectory,
                IsFolderPicker = true,
                DefaultDirectory = initialDirectory,
                EnsureFileExists = true,
                EnsurePathExists = true,
                EnsureValidNames = true,
                Multiselect = multiSelect
            };

            if (d.ShowDialog() == Microsoft.WindowsAPICodePack.Dialogs.CommonFileDialogResult.Ok) {
                    if (multiSelect) {
                        return d.FileNames;
                    } else {
                        return new[] { d.FileName };
                    }
                } else {
                    return new[] { String.Empty };
                }
        }

        public static string SelectDirectory(string title, string initialDirectory) => SelectDirectories(title, initialDirectory, false).Last();
        public static IEnumerable<string> SelectDirectories(string title) => SelectDirectories(title, null, true);

        public static void Replace<T>(this IList<T> list, T source, T newElement) {
            var i = list.IndexOf(source);
            list.Remove(source);
            list.Insert(i, newElement);
        }
    }
}
