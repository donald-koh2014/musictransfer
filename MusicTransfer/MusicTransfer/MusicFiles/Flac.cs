﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;

namespace MusicTransfer.MusicFiles {
    public enum BlockType : Int32 {
        StreamInfo = 0,
        Padding = 1,
        Application = 2,
        SeekTable = 3,
        VorbisComment = 4,
        CueSheet = 5,
        Picture = 6,
        Invalid = 127,
    }

    public class Flac : MusicFile {
        private static readonly char[] correctFlacStreamMarker = new[] { 'f', 'L', 'a', 'C' };
        private char[] flacStreamMarker = new char[4];
        private readonly List<MetadataBlock> metadataBlocks = new List<MetadataBlock>();

        public Flac(string filePath) {
            LoadFrom(filePath);
        }

        public string FlacStreamMarker { get { return new String(flacStreamMarker); } }

        public override MusicFileType MusicFileType => MusicFileType.Flac;

        protected override void LoadFromCore(FileStream stream) {
            var reader = new BinaryReader(stream);
            metadataBlocks.Clear();
            try {
                flacStreamMarker = CreateFrom(reader);
                do {
                    metadataBlocks.Add(MetadataBlock.CreateFrom(reader));
                } while ( !metadataBlocks.Last().Header.LastMetadataBlockFlag );
            } catch ( Exception e ) {
                throw new InvalidOperationException("正しいFLAC形式ではありません", e);
            }
        }

        private static char[] CreateFrom(BinaryReader reader) {
            var marker = new char[4];
            for ( int i = 0; i < 4; i++ ) {
                marker[i] = (char)reader.ReadSByte();
            }

            if ( !correctFlacStreamMarker.SequenceEqual(marker) ) {
                var msg = "FLACストリームマーカと異なります。(";
                foreach ( var m in marker ) {
                    msg += " " + ((int)m).ToString("X2");
                }
                throw new InvalidOperationException(msg);
            }
            
            return marker;
        }

        protected override void AddTags() {
            foreach ( var b in metadataBlocks ) {
                if ( b.Data is VorbisComment v ) {
                    TagList.AddRange(v.FlacTag);
                }
            }
        }
    }

    public struct MetadataBlock {
        public MetadataBlockHeader Header;
        public IMetadataBlockData Data;

        public static MetadataBlock CreateFrom(BinaryReader reader) {
            var block = new MetadataBlock {
                Header = MetadataBlockHeader.CreateFrom(reader)
            };
            block.Data = MetadataBlockFactory.CreateFrom(reader, block.Header);
            return block;
        }
    }

    public struct MetadataBlockHeader {
        public bool LastMetadataBlockFlag;
        public BlockType BlockType;
        public int MetadataLength;

        public static MetadataBlockHeader CreateFrom(BinaryReader reader) {
            var header = new MetadataBlockHeader();
            var b = reader.ReadByte();

            header.LastMetadataBlockFlag = (b & 0x8) == 0x8;
            header.BlockType = (BlockType)(b & 0x7);
            byte[] bigendian = new byte[4];
            reader.ReadBytes(3).CopyTo(bigendian, 1);
            header.MetadataLength = BitConverter.ToInt32(Tools.Tool.ChangeEndian(bigendian), 0);

            return header;
        }
    }

    public interface IMetadataBlockData {
    }

    public class MetadataBlockFactory {
        public static IMetadataBlockData CreateFrom(BinaryReader reader, MetadataBlockHeader header) {
            if ( header.BlockType == BlockType.VorbisComment ) {
                return VorbisComment.CreateFrom(reader);
            } else {
                return DummyBlockData.CreateFrom(reader, header.MetadataLength);
            }
        }
    }

    public struct DummyBlockData : IMetadataBlockData {
        public static IMetadataBlockData CreateFrom(BinaryReader reader, int length) {
            var dummy = new DummyBlockData();
            reader.BaseStream.Position += length;

            return dummy;
        }
    }

    public struct VorbisComment : IMetadataBlockData {
        public string VendorString; // TODO: プロパティにする
        public (string name, string value)[] FlacTag;

        public static IMetadataBlockData CreateFrom(BinaryReader reader) {
            var data = new VorbisComment();
            var tagList = new List<(string name, string value)>();

            var len = reader.ReadInt32();
            data.VendorString = Encoding.UTF8.GetString(reader.ReadBytes(len));

            var imax = reader.ReadInt32();
            for ( var i = 0; i < imax; i++ ) {
                len = reader.ReadInt32();
                var comment = Encoding.UTF8.GetString(reader.ReadBytes(len));
                var word = comment.Split(new[] { '=' }, 2);
                tagList.Add((word[0], word[1]));
            }

            data.FlacTag = tagList.ToArray();

            return data;
        }
    }
}
