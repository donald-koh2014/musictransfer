﻿namespace MusicTransfer.MusicFiles {
    interface ITag {
        (string TagName, string TagValue)[] Tags { get; }
        string GetTag(string tagName);
    }
}
