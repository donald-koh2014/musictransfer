﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using MusicTransfer.Tools;

namespace MusicTransfer.MusicFiles {
    public class M4a : MusicFile {
        private List<Box> boxes;

        public Box[] Boxes { get { return boxes?.ToArray(); } }

        public override MusicFileType MusicFileType => MusicFileType.M4a;

        public M4a(string filePath) {
            LoadFrom(filePath);
        }

        protected override void LoadFromCore(FileStream stream) {
            try {
                boxes = new List<Box>();
                while ( stream.Position < stream.Length ) {
                    boxes.Add(Box.CreateFrom(stream));
                }
            } catch ( EndOfStreamException ) {
            } catch ( Exception e ) {
                throw new Exception("M4Aファイルをロード中に例外が発生しました。", e);
            }
        }
        protected override void AddTags() {
            foreach ( var b in boxes ) {
                AddTags(b);
            }
        }

        private void AddTags(Box box) {
            if ( box is Mpeg4TagBox ) {
                var tb = box as Mpeg4TagBox;
                TagList.Add((tb.TagName, tb.TagValue));
            } else if ( box is ContainerBox ) {
                foreach ( var b in box.Boxes ) {
                    AddTags(b);
                }
            }
        }
    }

    public enum BoxType {
        UnknownBox = -1,
        FileTypeBox = 0,
        ContainerBox = 1,
        MetaBox = 2,
        Mpeg4TagBox = 3,
    }

    public abstract class Box {
        public ulong BoxLength { get; private set; }
        public BoxType BoxType { get; private set; }
        public string BoxTypeString { get; private set; }
        public ulong DataLength => BoxLength - 8;

        protected List<Box> BoxesList;
        public Box[] Boxes => BoxesList.ToArray();

        protected abstract void ReadDataFrom(FileStream stream);

        public static Box CreateFrom(FileStream stream) {
            var reader = new BinaryReader(stream);
            var length = (ulong)BitConverter.ToUInt32(Tool.ChangeEndian(reader.ReadBytes(4)), 0);
            var bytes = reader.ReadBytes(4);
            string type;


            if ( bytes[0] == 0xa9 ) {
                type = "©" + Encoding.ASCII.GetString(new[] { bytes[1], bytes[2], bytes[3] });
            } else {
                type = Encoding.ASCII.GetString(bytes);
            }

            Box box;
            switch ( type ) {
            case "ftyp":
                box = new FileTypeBox { BoxType = BoxType.FileTypeBox };
                break;
            case "moov":
            case "udta":
            case "ilst":
                box = new ContainerBox {
                    BoxType = BoxType.ContainerBox
                };
                break;
            case "meta":
                box = new MetaBox { BoxType = BoxType.MetaBox };
                break;
            case "trkn":
            case "disk":
            case "©alb":
            case "aART":
            case "©nam":
                box = new Mpeg4TagBox { BoxType = BoxType.Mpeg4TagBox };
                break;
            default:
                box = new UnknownBox { BoxType = BoxType.UnknownBox };
                break;
            }

            if ( length == 1 ) {
                length = BitConverter.ToUInt64(Tool.ChangeEndian(reader.ReadBytes(8)), 0);
            }
            box.BoxLength = length;
            box.BoxTypeString = type;
            box.ReadDataFrom(stream);

            return box;
        }

        protected static List<Box> ReadBoxes(FileStream stream, ulong length) {
            var readedBytes = 0L;

            var boxes = new List<Box>();
            while ( readedBytes < (long)length ) {
                boxes.Add(Box.CreateFrom(stream));
                readedBytes += (long)boxes.Last().BoxLength;
            }

            return boxes;
        }

        public override string ToString() => $"Type={BoxTypeString}";

    }

    public class UnknownBox : Box {
        protected override void ReadDataFrom(FileStream stream) {
            stream.Seek((long)DataLength, SeekOrigin.Current);
        }
    }

    public class FileTypeBox : Box {
        private string majorBrand;
        private int minorVersion;
        private string[] compatibleBrands;

        public string MajorBrand => majorBrand.Clone() as String;
        public int MinorVersion => minorVersion;
        public string[] CompatibleBrands => compatibleBrands.Clone() as String[];

        protected override void ReadDataFrom(FileStream stream) {
            var reader = new BinaryReader(stream);
            majorBrand = new String(reader.ReadChars(4));
            minorVersion = BitConverter.ToInt32(Tool.ChangeEndian(reader.ReadBytes(4)), 0);
            var compBrands = new List<String>();
            for ( var i = 0L; i < ((long)DataLength - 8) / 4; i++ ) {
                compBrands.Add(new String(reader.ReadChars(4)));
            }
            compatibleBrands = compBrands.ToArray();
        }

        public override string ToString() {
            return base.ToString() + $", MajorBrand={majorBrand}, MinorVer={minorVersion}, CompilationBrands=" + String.Join(",", compatibleBrands);
        }
    }

    public class ContainerBox : Box {
        protected override void ReadDataFrom(FileStream stream) {
            BoxesList = ReadBoxes(stream, DataLength);
        }

        public override string ToString() {
            return base.ToString() + ", Inner Boxes=" + String.Join(",", BoxesList);
        }
    }

    public class MetaBox : ContainerBox {
        protected override void ReadDataFrom(FileStream stream) {
            var reader = new BinaryReader(stream);
            _ = reader.ReadSByte();
            _ = reader.ReadBytes(3);
            BoxesList = ReadBoxes(stream, DataLength - 4);
        }
    }

    public class Mpeg4TagBox : Box {
        public string TagName { get; private set; }
        public string TagValue { get; private set; }
        protected override void ReadDataFrom(FileStream stream) {
            var reader = new BinaryReader(stream);
            TagName = BoxTypeString switch
            {
                "trkn" => "TRACKNUMBER",
                "disk" => "DISCNUMBER",
                "©alb" => "ALBUM",
                "aART" => "ALBUMARTIST",
                "©nam" => "TITLE",
                _ => BoxTypeString,
            };

            switch ( BoxTypeString ) {
            case "trkn":
            case "disk":
                reader.ReadBytes(18);
                TagValue = BitConverter.ToUInt16(Tool.ChangeEndian(reader.ReadBytes(2)), 0).ToString();
                reader.ReadBytes((int)DataLength - 20);
                break;
            default:
                _ = reader.ReadBytes(16);
                TagValue = Encoding.UTF8.GetString(reader.ReadBytes((int)DataLength - 16));
                break;
            }
        }
    }
}
