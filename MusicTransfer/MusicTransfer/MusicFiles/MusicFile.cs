﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MusicTransfer.MusicFiles {
    public enum MusicFileType {
        Unknown = -1,
        Flac = 0,
        M4a = 1,
    }

    public abstract class MusicFile : ITag {
        protected List<(string TagName, string TagValue)> TagList = new List<(string, string)>();

        public void LoadFrom(string filePath) {
            using var stream = File.OpenRead(filePath);
            try {
                LoadFromCore(stream);

                TagList.Clear();
                AddTags();
            } catch ( Exception e ) {
                throw new InvalidOperationException($"{filePath}は正しい{MusicFileType}形式ではありません", e);
            }
        }

        protected MusicFile() { }

        public static MusicFile CreateMusicFile(string filePath) {
            var fi = new FileInfo(filePath);
            if ( fi.Extension == ".flac" ) {
                return new Flac(filePath);
            } else if ( fi.Extension == ".m4a" ) {
                return new M4a(filePath);
            }

            throw new ArgumentException($"ファイル\"{filePath}\"は音楽ファイルではありません。");
        }

        protected abstract void AddTags();

        public abstract MusicFileType MusicFileType { get; }

        protected abstract void LoadFromCore(FileStream stream);

        #region ITag Member
        public (string TagName, string TagValue)[] Tags => TagList.ToArray();

        public string GetTag(string tagName) {
            var tg = TagList.Where(t => String.Compare(t.TagName, tagName, true) == 0).Select(t => t.TagValue);

            if ( 0 < tg.Count() ) {
                return tg.First().Replace("\r\n", "").Replace("\n", "");
            } else {
                return String.Empty;
            }
        }
        #endregion
    }
}
