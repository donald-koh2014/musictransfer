﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using Microsoft.Win32;
using MusicTransfer.Properties;
using NLog;

namespace MusicTransfer {
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly Settings settings = Settings.Default;

        private readonly Engine mflist = new Engine();

        public MainWindow() {
            InitializeComponent();

            logger.Trace("WPF コンポーネントを初期化しました。");

            mflist.UpdatedTransferredInfo += (ss, ee) => {
                var pbDispatcher = ProgressBarTransfer.Dispatcher;
                pbDispatcher.BeginInvoke((Action)(() => {
                    ProgressBarTransfer.Minimum = 0;
                    ProgressBarTransfer.Maximum = ee.TransferInfo.TotalSize;
                    ProgressBarTransfer.Value = ee.TransferInfo.TransferredSize;
                }));
                TextBoxTranserMusicFile.Dispatcher.BeginInvoke((Action)(() => {
                    var mfi = ee.TransferInfo.MusicFileInfo;
                    TextBoxTranserMusicFile.Text = $"{mfi.Album}: {mfi.Title}";
                }));
            };

            mflist.CatchedPathToLongException += (ss, ee) => TextBoxErrorLog.Dispatcher.BeginInvoke((Action)(() => TextBoxErrorLog.Text += $"パスが長すぎるため、操作は無視されます。: {ee.Path}"));
            mflist.IgnoredMusicFile += (ss, ee) => TextBoxErrorLog.Dispatcher.BeginInvoke((Action)(() => TextBoxErrorLog.Text += $"パスが見つからないため、転送しません。: Album={ee.MusicFileInfo.Album}, Tilte={ee.MusicFileInfo.Title}"));

            logger.Trace("転送結果が更新されたときのイベントを追加しました。");

            if ( String.IsNullOrEmpty(settings.MusicFileListFile) ) {
                settings.MusicFileListFile = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "MusicTransfer.lst");
            }

            if ( settings.MusicFileFolders == null ) {
                settings.MusicFileFolders = new System.Collections.Specialized.StringCollection();
            }

            if ( settings.MusicFileFolders.Count < 1 ) {
                settings.MusicFileFolders.Add(Environment.GetFolderPath(Environment.SpecialFolder.MyMusic));
                settings.MusicFileFolders.Add(Environment.GetFolderPath(Environment.SpecialFolder.CommonMusic));
            }

            CheckBoxAutoSync.IsChecked = settings.AutoSync;

            if ( File.Exists(settings.MusicFileListFile) ) {
                mflist.LoadFrom(settings.MusicFileListFile);
            }
        }


        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            settings.Save();
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e) {
            IsEnabled = false;

            ListBoxMusicFileFolders.Items.Clear();
            foreach ( var f in settings.MusicFileFolders ) {
                ListBoxMusicFileFolders.Items.Add(f);
            }

            if ( CheckBoxAutoSync.IsChecked == true ) {
                await mflist.SyncMusicFolders();
            }

            IsEnabled = true;
        }

        private void ButtonAddMusicFileFolders_Click(object sender, RoutedEventArgs e) {
            var ds = Tools.Tool.SelectDirectories("Select Music Folders");
            foreach ( var d in ds ) {
                if ( settings.MusicFileFolders.Contains(d) ) {
                    MessageBox.Show($"Selected music folder \"{d}\" is already added!", "Invalid music folder",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                } else {
                    settings.MusicFileFolders.Add(d);
                    ListBoxMusicFileFolders.Items.Add(d);
                }
            }
        }

        private void ButtonDeleteMusicFileFolders_Click(object sender, RoutedEventArgs e) {
            while ( 0 < ListBoxMusicFileFolders.SelectedItems.Count ) {
                var item = ListBoxMusicFileFolders.SelectedItems[0] as String;
                settings.MusicFileFolders.Remove(item);
                ListBoxMusicFileFolders.Items.Remove(item);
            }
        }

        private void BrowseDestinationFolder() {
            var d = Tools.Tool.SelectDirectory("Select Destination Folder", settings.DestinationFolder);
            if ( d != String.Empty ) {
                settings.DestinationFolder = d;
                if ( !settings.ExclusionFolder.Contains(d) ) {
                    settings.ExclusionFolder = String.Empty;
                }
            }
        }

        private void ButtonChangeDestinationFolder_Click(object sender, RoutedEventArgs e) {
            BrowseDestinationFolder();
        }

        private void ButtonChangeExclusionFolder_Click(object sender, RoutedEventArgs e) {
            var d = Tools.Tool.SelectDirectory("Select ExclusionFolder", settings.ExclusionFolder);
            if ( d != String.Empty ) {
                if ( d.Contains(settings.DestinationFolder) ) {
                    settings.ExclusionFolder = d;
                } else {
                    MessageBox.Show("Selected exclusion folder isn't in destination folder!", "Invalid exclusion folder",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void OnPreviewKeyDown(object sender, KeyEventArgs e) {
            var tb = sender as TextBox;

            if ( e.Key != Key.Delete ) {
                return;
            }

            if ( tb.Equals(TextBoxDestinationFolder) ) {
                settings.DestinationFolder = String.Empty;
            }

            settings.ExclusionFolder = String.Empty;
        }

        private void OnGotFocus(object sender, RoutedEventArgs e) {
            if ( sender is TextBox ) {
                var tb = sender as TextBox;
                Dispatcher.InvokeAsync(() => {
                    Task.Yield();
                    tb.SelectAll();
                });
            }
        }

        private async void ButtonSync_Click(object sender, RoutedEventArgs e) {
            IsEnabled = false;
            await mflist.SyncMusicFolders();
            IsEnabled = true;
        }

 

        private void CheckBoxAutoSync_Click(object sender, RoutedEventArgs e) {
            // TODO: Bindingにする
            settings.AutoSync = CheckBoxAutoSync.IsChecked == true;
        }

        private long GetTransableSize() {
            var di = new DriveInfo(System.IO.Path.GetPathRoot(settings.DestinationFolder));
            var tsize = di.IsReady ? di.AvailableFreeSpace : 0;

            var s = settings.MaximumTransferSize.ToUpper();
            if ( s == "無制限" ) {
                return 0;
            }

            var factor = 1.0;
            var ui = 0;
            if ( !String.IsNullOrEmpty(s) ) {
                if ( s.EndsWith("KB") ) {
                    factor = 1000.0;
                    ui = 2;
                } else if ( s.EndsWith("KIB") ) {
                    factor = 1024.0;
                    ui = 3;
                } else if ( s.EndsWith("MB") ) {
                    factor = Math.Pow(1000.0, 2.0);
                    ui = 2;
                } else if ( s.EndsWith("MIB") ) {
                    factor = Math.Pow(1024.0, 2.0);
                    ui = 3;
                } else if ( s.EndsWith("GB") ) {
                    factor = Math.Pow(1000.0, 3.0);
                    ui = 2;
                } else if ( s.EndsWith("GIB") ) {
                    factor = Math.Pow(1024.0, 3.0);
                    ui = 3;
                } else {
                    return -1;
                }
            }

            if ( (0 < s.Length) && Double.TryParse(s.Remove(s.Length - ui), out var size) ) {
                size *= factor;
                return size < tsize ? (long)size : tsize;
            } else {
                return -1;
            }
        }

        private void SystemEvents_SessionEnding(object sender, SessionEndingEventArgs e) {
            switch ( e.Reason ) {
            case SessionEndReasons.Logoff:
            case SessionEndReasons.SystemShutdown:
                e.Cancel = true;
                break;
            }
        }

        private async void ButtonTransfer_Click(object sender, RoutedEventArgs e) {
            IsEnabled = false;

            if ( String.IsNullOrWhiteSpace(settings.DestinationFolder) ) {
                BrowseDestinationFolder();
            }

            if ( !Directory.Exists(settings.DestinationFolder) ) {
                switch (MessageBox.Show($"転送先フォルダ ({settings.DestinationFolder}) が存在しません。作成しますか?\r\nOK: 作成し、転送を行う。\r\nCancel: 転送をキャンセルする。", "転送先フォルダを作成する", MessageBoxButton.OKCancel) ) {
                case MessageBoxResult.OK:
                    try {
                        Directory.CreateDirectory(settings.DestinationFolder);
                    } catch ( DirectoryNotFoundException ex ) {
                        logger.Error(ex);
                        MessageBox.Show($"転送先フォルダ {settings.DestinationFolder} を作成できませんでした。転送を中止します。", "転送先ディレクトリ作成失敗", MessageBoxButton.OK, MessageBoxImage.Error);
                        IsEnabled = true;
                        return;
                    }
                    break;
                default:
                    IsEnabled = true;
                    return;
                }
            }

            if ( MessageBox.Show($"フォルダ{settings.DestinationFolder}は、{settings.ExclusionFolder}以外すべて削除されます。\r\n転送を開始しますか?", "転送確認",
                MessageBoxButton.OKCancel) == MessageBoxResult.OK ) {
                var ts = GetTransableSize();
                if ( 0 <= ts ) {
                    Cursor = Cursors.Wait;
                    SystemEvents.SessionEnding += new SessionEndingEventHandler(SystemEvents_SessionEnding);
                    await mflist.TrasferMusicFile(ts);
                    SystemEvents.SessionEnding -= new SessionEndingEventHandler(SystemEvents_SessionEnding);
                    Cursor = Cursors.Arrow;
                    MessageBox.Show("転送が完了しました");
                    TextBoxTranserMusicFile.Text = "";
                } else {
                    MessageBox.Show("最大転送サイズの指定が誤っています。", "最大転送サイズ", MessageBoxButton.OK, MessageBoxImage.Hand);
                }
            }
            IsEnabled = true;
        }
    }
}
