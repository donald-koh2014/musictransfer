﻿namespace MusicTransfer {
    public struct TransferInfo {
        public MusicFileInfo MusicFileInfo;
        public long TotalSize;
        public long TransferredSize;
    }
}
