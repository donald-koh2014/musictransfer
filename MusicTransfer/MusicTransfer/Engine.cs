﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using MusicTransfer.MusicFiles;
using MusicTransfer.Properties;

namespace MusicTransfer {
    public delegate void UpdatedTransferredInfoHandler(object sender, UpdateTransferredInfoEventArg e);
    public delegate void CatchedPathExceptionHandler(object sender, CatchedPathExceptionEventArg e);
    public delegate void IgnoreMusicFileInfoEventHandler(object sender, IgnoreMusicFileEventArg e);

    public class Engine {
        private static readonly Settings settings = Settings.Default;
        public DateTime LastUpdate { get; private set; }
        public List<MusicFileInfo> MusicFileList { get; private set; }
        public string MusicListFilePath { get; private set; }
        public event UpdatedTransferredInfoHandler UpdatedTransferredInfo;
        public event CatchedPathExceptionHandler CatchedPathToLongException;
        public event IgnoreMusicFileInfoEventHandler IgnoredMusicFile;

        public Engine() {
            MusicFileList = new List<MusicFileInfo>();
        }

        /// <summary>
        /// 指定したパスから音楽ファイルリストを読み込む。
        /// </summary>
        /// <param name="path">音楽ファイルリストを保存しているファイルのパス</param>
        public void LoadFrom(string path) {
            using var s = new FileStream(path, FileMode.Open);
            var reader = new StreamReader(s, Encoding.UTF8);
            LastUpdate = DateTime.Parse(reader.ReadLine());

            MusicFileList.Clear();
            MusicFileList.AddRange(MusicTransfer.MusicFileInfo.ReadFrom(reader));
        }

        public void SaveTo(string path, DateTime lastUpdate) {
            using var s = new FileStream(path, FileMode.Create);
            var writer = new StreamWriter(s, Encoding.UTF8);
            writer.WriteLine(lastUpdate.ToString("yyyy/MM/dd HH:mm:ss"));
            writer.Flush();

            MusicFileInfo.WriteTo(s, MusicFileList);
        }
        public void UpdateMusicFileListBy(string fileName) {
            FileInfo fi;
            MusicFile mf;

            try {
                fi = new FileInfo(fileName);
                mf = MusicFile.CreateMusicFile(fileName);
            } catch ( PathTooLongException ) {
                CatchedPathToLongException?.Invoke(this, new CatchedPathExceptionEventArg(fileName));
                return;
            } catch ( Exception e) {
                Debug.WriteLine(e.Message + GetInfo());
                return;
            }

            var album = mf.GetTag("ALBUM");
            Int32.TryParse(mf.GetTag("DISCNUMBER"), out int discno);
            Int32.TryParse(mf.GetTag("TRACKNUMBER"), out int trackno);
            var mfi = MusicFileList.Where(m => (m.Album == album) && (m.DiscNumber == discno) && (m.TrackNumber == trackno));
            if ( 0 < mfi.Count() ) {
                ReplaceToCurrentPath(mfi.First(), fileName);
            } else {
                MusicFileList.Add(new MusicFileInfo(mf, fileName, fi.Length));
            }
        }

        private void ReplaceToCurrentPath(MusicFileInfo musicFileInfo, string fileName) {
            if ( musicFileInfo.Path != fileName ) {
                var i = MusicFileList.IndexOf(musicFileInfo);
                MusicFileList.Remove(musicFileInfo);
                musicFileInfo.Path = fileName;
                MusicFileList.Insert(i, musicFileInfo);
            }
        }

        public void SetPathToEmpty(MusicFileInfo musicFileInfo) {
            ReplaceToCurrentPath(musicFileInfo, String.Empty);
        }

        private void IncrementCopiedCount(MusicFileInfo mfi) {
            var i = MusicFileList.IndexOf(mfi);
            MusicFileList.Remove(mfi);
            mfi.CopiedCount++;
            mfi.LastCopiedDateTime = DateTime.Now;
            MusicFileList.Insert(i, mfi);
        }

        private static List<MusicFileInfo> GetExclusionMusicFiles() {
            var dir = settings.ExclusionFolder;
            

            string[] files;
            try {
                files = String.IsNullOrEmpty(dir) ? new string[0] : Directory.GetFiles(dir, "*.*", SearchOption.AllDirectories);
            } catch ( DirectoryNotFoundException ) {
                files = new string[0];
            }

            var musicFiles = new List<MusicFileInfo>();
            foreach ( var f in files ) {
                try {
                    var fi = new FileInfo(f);
                    var mf = MusicFile.CreateMusicFile(f);
                    musicFiles.Add(new MusicFileInfo(mf, f, fi.Length));
                } catch ( Exception e ) {
                    Debug.WriteLine(e.Message + GetInfo());
                }
            }

            return musicFiles;
        }

        private static string GetInfo([CallerFilePath] string filePath = "", [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0) {
            return $"L{lineNumber} in {filePath}/{memberName}";
        }


        private static void RemoveMusicFiles() {
            var df = settings.DestinationFolder;
            var di = new DirectoryInfo(df);

            RemoveMusicFilesFrom(di);
        }

        private static void RemoveMusicFilesFrom(DirectoryInfo directory) {
            foreach ( var f in directory.GetFiles() ) {
                if ( f.Attributes.HasFlag(FileAttributes.ReadOnly) ) {
                    f.Attributes = FileAttributes.Normal;
                }
                f.Delete();
            }

            foreach ( var d in directory.GetDirectories() ) {
                if ( Path.GetFullPath(d.FullName).ToLower() != Path.GetFullPath(settings.ExclusionFolder).ToLower() ) {
                    RemoveMusicFilesFrom(d);
                    d.Delete();
                }
            }
        }

        private List<AlbumInfo> GetAlbumList() {
            var albums = new List<AlbumInfo>();

            foreach ( var a in MusicFileList.Select(mfi => mfi.Album).Distinct() ) {
                albums.Add(new AlbumInfo(a, MusicFileList.Where(mfi => mfi.Album == a).Select(mfi => mfi.CopiedCount).Average()));
            }

            return albums;
        }

        public Task TrasferMusicFile(long transferSize) {
            var t = new Task(() => {
                RemoveMusicFiles();

                var di = new DriveInfo(Path.GetPathRoot(settings.DestinationFolder));
                transferSize = di.IsReady ? (transferSize == 0 ? di.AvailableFreeSpace * 99 / 100 : transferSize) : 0;

                var transferList = new List<MusicFileInfo>();
                var exclusions = GetExclusionMusicFiles();

                var albums = GetAlbumList();

                foreach ( var c in albums.Select(a => a.CopiedCount).Distinct().OrderBy(c => c) ) {
                    var selected = SelectMusicFileInfos(albums, c, exclusions);
                    if ( (selected.Select(s => s.FileSize).Sum() + transferList.Select(s => s.FileSize).Sum()) < transferSize ) {
                        transferList.AddRange(selected);
                    } else {
                        var albumList = selected.Select(s => s.Album).Distinct();
                        var random = albumList.OrderBy(a => Guid.NewGuid());
                        foreach ( var r in random ) {
                            var a = MusicFileList.Where(mfi => mfi.Album == r);
                            if ( (a.Select(e => e.FileSize).Sum() + transferList.Select(s => s.FileSize).Sum()) < transferSize ) {
                                transferList.AddRange(a);
                            }
                        }
                    }
                }

                CopyMusicFile(transferList);
            });

            t.Start();
            return t;
        }

        private List<MusicFileInfo> SelectMusicFileInfos(IList<AlbumInfo> albumInfos, double copiedCount, IList<MusicFileInfo> exclusions) {
            var selectedAlbums = new List<MusicFileInfo>();
            var albums = albumInfos.Where(i => i.CopiedCount == copiedCount);
            foreach ( var a in albums ) {
                var candidate = MusicFileList.Where(mfi => (mfi.Album == a.Album));
                if ( candidate.Count() != candidate.Where(c => String.IsNullOrEmpty(c.Path)).Count() ) {
                    foreach ( var c in candidate.Where(c => String.IsNullOrEmpty(c.Path)) ) {
                        IgnoredMusicFile?.Invoke(this, new IgnoreMusicFileEventArg(c));
                    }
                }
                foreach ( var c in candidate.Where(cc => !String.IsNullOrEmpty(cc.Path)) ) {
                    foreach ( var f in settings.MusicFileFolders ) {
                        if ( c.Path.Contains(f) && !c.IsExclusion
                            && exclusions.Where(e => (e.Album == c.Album)
                                                && (e.DiscNumber == c.DiscNumber) && (e.TrackNumber == c.TrackNumber)).Count() < 1 ) {
                            selectedAlbums.Add(c);
                        }
                    }
                }
            }

            return selectedAlbums;
        }

        private void CopyMusicFile(IList<MusicFileInfo> list) {
            var df = new DirectoryInfo(settings.DestinationFolder);
            var transferredInfo = new TransferInfo {
                TotalSize = list.Select(s => s.FileSize).Sum(),
                TransferredSize = 0
            };


            foreach ( var mfi in list ) {
                var source = new FileInfo(mfi.Path);
                var parent = new DirectoryInfo(Path.GetDirectoryName(mfi.Path));
                var parent2 = new DirectoryInfo(parent.Parent.Name);

                var di = new DirectoryInfo(Path.Combine(df.FullName, parent2.Name, parent.Name));
                if ( !di.Exists ) {
                    di.Create();
                }
                var fi = System.IO.Path.Combine(di.FullName, source.Name);
                transferredInfo.TransferredSize += mfi.FileSize;
                transferredInfo.MusicFileInfo = mfi;
                source.CopyTo(fi);
                UpdatedTransferredInfo?.Invoke(this, new UpdateTransferredInfoEventArg(transferredInfo));

                IncrementCopiedCount(mfi);
                SaveTo(settings.MusicFileListFile, DateTime.Now);
            }
        }

        public Task SyncMusicFolders() {
            var t = new Task(() => {
                var ud = DateTime.Now;

                var notFoundPath = new List<MusicFileInfo>();
                foreach ( var m in MusicFileList ) {
                    notFoundPath.Add(m);
                }

                foreach ( var nfp in notFoundPath ) {
                    SetPathToEmpty(nfp);
                }

                foreach ( var d in settings.MusicFileFolders ) {
                    Debug.WriteLine($"フォルダ{d}を走査中");
                    foreach ( var f in Directory.EnumerateFiles(d, "*", SearchOption.AllDirectories) ) {
                        Debug.WriteLine($"{f}を更新中");
                        UpdateMusicFileListBy(f);
                    }
                }

                SaveTo(settings.MusicFileListFile, ud);
            });
            t.Start();
            return t;
        }
    }

    public class UpdateTransferredInfoEventArg : EventArgs {
        public TransferInfo TransferInfo { get; private set; }
        private UpdateTransferredInfoEventArg() { }
        public UpdateTransferredInfoEventArg(TransferInfo info) {
            TransferInfo = info;
        }
    }

    public class CatchedPathExceptionEventArg : EventArgs {
        public string Path { get; private set; }
        private CatchedPathExceptionEventArg() { }
        public CatchedPathExceptionEventArg(string path) {
            Path = path;
        }
    }

    public class IgnoreMusicFileEventArg : EventArgs {
        public MusicFileInfo MusicFileInfo { get; private set; }
        private IgnoreMusicFileEventArg() { }
        public IgnoreMusicFileEventArg(MusicFileInfo musicFileInfo) {
            MusicFileInfo = musicFileInfo;
        }
    }
}
