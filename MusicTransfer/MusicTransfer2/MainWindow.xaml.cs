﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;

using NLog;
using MusicTransfer2.Music;
using MusicTransfer2.Tools;
using static MusicTransfer2.Music.MusicFile;
using System.Windows.Controls.Primitives;

namespace MusicTransfer2 {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        private readonly static Properties.Settings settings = Properties.Settings.Default;
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();

        private readonly MusicFileList list;

        private readonly MainWindowModelView modelView = new MainWindowModelView();
        private CancellationTokenSource? ctsSyncTask = null;
        private CancellationTokenSource? ctsTransferTask = null;

        public MainWindow() {
            InitializeComponent();
            if ( String.IsNullOrWhiteSpace(settings.MusicFileListPath) ) {
                settings.MusicFileListPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "musicfile.xml");
            }

            if (!File.Exists(settings.MusicFileListPath)) {
                File.WriteAllText(settings.MusicFileListPath, Properties.Resources.musicfile);
            }

            list = new MusicFileList(settings.MusicFileListPath);
            modelView.AlbumCount.Value = list.Albums.Count();
            modelView.TrackCount.Value = list.MusicFiles.Count();
            list.Transfered += (_, e) => modelView.TransferedSize.Value = modelView.TransferSizeUnit.Value switch {
                "TB" => e / (long)SizeUnit.TB,
                "GB" => e / (long)SizeUnit.GB,
                _ => e / (long)SizeUnit.MB,
            };
            list.MaxTransferSize += (_, e) => {
                if ( (long)SizeUnit.TB < e ) {
                    modelView.TransferSize.Value = e / (long)SizeUnit.TB;
                    modelView.TransferSizeUnit.Value = SizeUnit.TB.ToString();
                } else if ( (long)SizeUnit.GB < e ) {
                    modelView.TransferSize.Value = e / (long)SizeUnit.GB;
                    modelView.TransferSizeUnit.Value = SizeUnit.GB.ToString();
                } else {
                    modelView.TransferSize.Value = e / (long)SizeUnit.MB;
                    modelView.TransferSizeUnit.Value = SizeUnit.MB.ToString();
                }
            };
            list.Transfering += (_, mf) => modelView.TransferingMusicFile.Value = $"アルバム {mf.Album} - タイトル {mf.Title}";

            DataContext = modelView;
            logger.Info("Initialized the application main window.");
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            settings.Save();
            logger.Info($"Saved the application setting file.");
            logger.Info("Closing the application.");
        }

        private void ButtonAddMusicFileFolders_Click(object sender, RoutedEventArgs e) {
            var selected = Tool.SelectDirectories("Select Music File Folder(s).");
            foreach ( var s in selected ) {
                if ( !settings.MusicFileFolders.Contains(s) ) {
                    _ = settings.MusicFileFolders.Add(s);
                }
            }
            ListBoxMusicFileFolders.Items.Refresh();
        }

        private void ButtonDeleteMusicFileFolders_Click(object sender, RoutedEventArgs e) {
            foreach ( string s in ListBoxMusicFileFolders.SelectedItems ) {
                settings.MusicFileFolders.Remove(s);
            }
            ListBoxMusicFileFolders.Items.Refresh();
        }

        private void ButtonBrowseDestinationFolder_Click(object sender, RoutedEventArgs e) {
            var selected = Tool.SelectDirectory("Select Destination Folder.", settings.DestinationFolder);
            if ( !String.IsNullOrWhiteSpace(selected) ) {
                settings.DestinationFolder = selected;
            }
        }

        private void ButtonAddExclusionFolders_Click(object sender, RoutedEventArgs e) {
            var selected = Tool.SelectDirectories("Select Exclusion Folder(s).");
            foreach ( var s in selected ) {
                if ( !settings.ExclusionFolders.Contains(s) ) {
                    _ = settings.ExclusionFolders.Add(s);
                }
            }
            ListBoxExclusionFolders.Items.Refresh();
        }

        private void ButtonDeleteExclusionFolders_Click(object sender, RoutedEventArgs e) {
            foreach ( string s in ListBoxExclusionFolders.SelectedItems ) {
                settings.ExclusionFolders.Remove(s);
            }
            ListBoxExclusionFolders.Items.Refresh();
        }

        private async void ToggleButtonSync_Checked(object sender, RoutedEventArgs e) {
            try {
                ctsSyncTask = new CancellationTokenSource();

                modelView.SetIsSynchronizing(true);
                await Task.Run(async () => {
                    var sw = new Stopwatch();
                    sw.Start();

                    foreach ( var d in settings.MusicFileFolders ) {
                        if ( d is not null ) {
                            await foreach ( var file in GetMusicFilesAsync(d) ) {
                                if ( ctsSyncTask.Token.IsCancellationRequested ) {
                                    break;
                                }
                                try {
                                    var mf = MusicFile.Load(file);
                                    logger.Trace($"{file} のアルバム名は {mf.Album.Name} で、アルバムアーティストは {mf.Album.Artist} です。");
                                    list.UpdateMusicFile(mf);
                                } catch ( InvalidOperationException ioe ) {
                                    logger.Error(ioe);
                                } catch ( Exception ) {
                                    throw;
                                }
                            };
                        }
                    }
                    list.SaveMusicFileList();
                    sw.Stop();
                    logger.Trace($"Elaped time = {sw.ElapsedMilliseconds} ms.");
                    
                }, ctsSyncTask.Token);
                modelView.SetIsSynchronizing(false);
            } catch {
            }
        }

        private void ToggleButtonSync_Unchecked(object sender, RoutedEventArgs e) {
            ctsSyncTask?.Cancel();
        }

        private async static Task RemoveMusicFileAsync(DirectoryInfo directory, CancellationToken token) {
            token.ThrowIfCancellationRequested();

            foreach ( var f in directory.GetFiles() ) {
                token.ThrowIfCancellationRequested();
                logger.Info($"ファイル {f.FullName} を削除します。");
                if ( f.Attributes.HasFlag(FileAttributes.ReadOnly) ) {
                    f.Attributes = FileAttributes.Normal;
                }
                f.Delete();
                logger.Trace($"ファイル {f.FullName} を削除しました。");
            }

            foreach ( var d in directory.GetDirectories() ) {
                token.ThrowIfCancellationRequested();
                if ( !settings.ExclusionFolders.Contains(d.FullName) ) {
                    logger.Info($"ディレクトリ {d.FullName} を削除します。");
                    await RemoveMusicFileAsync(d, token);
                    d.Delete();
                    logger.Trace($"ディレクトリ {d.FullName} を削除しました。");
                } else {
                    logger.Info($"ディレクトリ {d.FullName} は除外フォルダリストにあるため削除しません。");
                }
            }
        }

        private static long GetAvailableTransferSize() {
            var availableSize = (long)(new DriveInfo(System.IO.Path.GetPathRoot(settings.DestinationFolder) ?? throw new InvalidOperationException()).AvailableFreeSpace * 0.9);
            long transferSize = (long)(settings.TransferSizeUnit switch {
                "KB" => 1000,
                "MB" => 1000000,
                "GB" => 1000000000,
                "TB" => 1000000000000,
                _ => throw new InvalidOperationException("単位指定が不正です。")
            } * settings.MaximumTransferSize);
            transferSize = settings.TransferSizeMaximum ? availableSize : transferSize;
            return availableSize < transferSize ? availableSize : transferSize;
        }

        private async void ToggleButtonTransfer_Click(object sender, RoutedEventArgs e) {
            var isChecked = ToggleButtonTransfer.IsChecked ?? throw new InvalidOperationException();

            if ( isChecked ) {
                logger.Trace("転送を開始します。");
                modelView.SetIsTransfering(true);
                ctsTransferTask?.Dispose();
                ctsTransferTask = new CancellationTokenSource();
                try {
                    logger.Info("転送先フォルダを削除します。");
                    await RemoveMusicFileAsync(new DirectoryInfo(settings.DestinationFolder), ctsTransferTask.Token);
                    logger.Info("転送先フォルダを削除完了しました。");
                    logger.Info("音楽ファイルの転送を開始します。");
                    var transferSize = GetAvailableTransferSize();
                    logger.Info($"最大転送サイズは {transferSize} バイトです。");
                    await list.TransferAsync(settings.DestinationFolder, transferSize, ctsTransferTask.Token);
                    logger.Info("音楽ファイルの転送を完了しました。");
                    list.SaveMusicFileList();
                } catch ( TaskCanceledException ) {
                    logger.Info("転送を中止しました。");
                } catch ( Exception ex ) {
                    logger.Error(ex, "転送処理中に例外が発生しました。");
                    throw;
                } finally {
                    modelView.TransferingMusicFile.Value = String.Empty;
                    modelView.SetIsTransfering(false);
                }
            } else {
                logger.Info("転送の中止を要求しました。");
                ctsTransferTask?.Cancel();
            }
        }
    }
}
