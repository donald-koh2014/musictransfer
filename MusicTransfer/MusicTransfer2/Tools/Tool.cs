﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicTransfer2.Tools {
    internal static class Tool {
        private static IEnumerable<string> SelectDirectories(string title, string initialDirectory, bool multiSelect) {
            using var d = new Microsoft.WindowsAPICodePack.Dialogs.CommonOpenFileDialog {
                Title = title,
                InitialDirectory = initialDirectory,
                IsFolderPicker = true,
                DefaultDirectory = initialDirectory,
                EnsureFileExists = true,
                EnsurePathExists = true,
                EnsureValidNames = true,
                Multiselect = multiSelect
            };

            if ( d.ShowDialog() == Microsoft.WindowsAPICodePack.Dialogs.CommonFileDialogResult.Ok ) {
                if ( multiSelect ) {
                    return d.FileNames;
                } else {
                    return new[] { d.FileName };
                }
            } else {
                return new[] { String.Empty };
            }
        }

        public static string SelectDirectory(string title, string initialDirectory) {
            return SelectDirectories(title, initialDirectory, false).Last();
        }

        public static IEnumerable<string> SelectDirectories(string title) {
            return SelectDirectories(title, String.Empty, true);
        }

        public static IEnumerable<byte> ChangeEndian(this IEnumerable<byte> bytes) {
            var c = bytes.Count();
            for ( var i = 0; i < c; i++ ) {
                yield return bytes.ElementAt(c - i - 1);
            }
        }
    }
}
