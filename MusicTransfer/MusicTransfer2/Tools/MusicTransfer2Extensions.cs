﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MusicTransfer2.Tools {
    internal static class MusicTransfer2Extensions {
        private static readonly Random random = new Random();

        public static IEnumerable<T> ToEnumerable<T>(this IEnumerable<T> list) {
            foreach (var item in list) {
                yield return item;
            }
        }

        public static IEnumerable<T> ToRandom<T>(this IEnumerable<T> list) {
            return list.OrderBy(element => random.Next());
        }

        public static long ToByte(this Music.MusicFileInfo fileInfo) {
            return (long)(fileInfo.Size * (long)fileInfo.SizeUnit);
        }

        public static string StringAttribute(this XElement element, in string attributeName, in bool isRequired = true, in string defaultValue = "") {
            var att = element.Attribute(attributeName);

            if ( att is null ) {
                if ( isRequired ) {
                    throw new InvalidOperationException($"要素 {element.Name} に属性 {attributeName} がありません。");
                } else {
                    return defaultValue;
                }
            }

            return att.Value ?? "";
        }

        public static T ParsableAttribute<T>(this XElement element, in string attributeName, in bool isRequired = true, in T defaultValue = default) where T : struct {
            var att = element.StringAttribute(attributeName, isRequired);

            if ( String.IsNullOrWhiteSpace(att) && !isRequired ) {
                return defaultValue;
            }

            var ttype = typeof(T);

            try {
                if ( ttype == typeof(int) ) {
                    return (T)(object)Int32.Parse(att);
                } else if ( ttype == typeof(double) ) {
                    return (T)(object)Double.Parse(att);
                } else if ( ttype == typeof(DateTime) ) {
                    return (T)(object)DateTime.Parse(att);
                } else {
                    throw new NotSupportedException($"型 {ttype} はサポートされていません。");
                }
            } catch {
                throw new InvalidOperationException($"要素 {element.Name} の属性 {attributeName} の値 {att} を整数に変換できませんでした。");
            }
        }

        public static bool YesNoAttribute(this XElement element, in string attributeName, in bool isRequired = true, in bool defaultValue = false) {
            var att = element.StringAttribute(attributeName, isRequired);

            if ( String.IsNullOrWhiteSpace(att) && !isRequired ) {
                return defaultValue;
            }

            return att switch {
                "yes" => true,
                "no" => false,
                _ => throw new InvalidOperationException($"要素 {element.Name} の属性 {attributeName} の値 {att} は \"yes\" か \"no\" である必要があります。"),
            };
        }

        public static string StringElement(this XElement element, in XName elementName, in bool isRequired = true, in string defaultValue = "") {
            var elm = element.Element(elementName);

            if ( elm is null ) {
                if ( isRequired ) {
                    throw new InvalidOperationException($"要素 {element.Name} に要素 {elementName} がありません。");
                } else {
                    return defaultValue;
                }
            }

            return elm.Value ?? "";
        }

        public static IEnumerable<XElement> EnumerableElement(this XElement? element, in XName elementName) {
            return element?.Elements(elementName) ?? Enumerable.Empty<XElement>();
        }
    }
}
