﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

using MusicTransfer2.Tools;

namespace MusicTransfer2.Music {
    internal class Album : IEqualityComparer<Album> {
        public string Name { get; }
        public string Artist { get; }
        public Album(in string name, in string artist) {
            Name = name;
            Artist = artist;
        }

        public static IEnumerable<Album> Load(XElement xelement) {
            var ns = xelement.Name.Namespace;
            foreach (var element in xelement.Element(ns + "albums").EnumerableElement(ns + "album")) {
                yield return new Album(element.StringAttribute("name"), element.StringAttribute("artist", false));
            }
        }

        public override bool Equals(object? obj) {
            return obj is Album album &&
                   Name == album.Name &&
                   Artist == album.Artist;
        }

        public override int GetHashCode() {
            return HashCode.Combine(Name, Artist);
        }

        public bool Equals(Album? x, Album? y) {
            return x is not null &&
                y is not null &&
                x.Equals(y);
        }

        public int GetHashCode([DisallowNull] Album obj) {
            return HashCode.Combine(obj.Name, obj.Artist);
        }
    }
}
