﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using MusicTransfer2.Tools;
using NLog;

namespace MusicTransfer2.Music {
    internal class MusicFileList {
        public event EventHandler<long>? Transfered;
        public event EventHandler<long>? MaxTransferSize;
        public event EventHandler<MusicFile>? Transfering;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly string listPath;
        private readonly List<Album> albums;
        private readonly List<MusicFile> musicFiles;

        public IEnumerable<Album> Albums => albums.ToEnumerable();
        public IEnumerable<MusicFile> MusicFiles => musicFiles.ToEnumerable();

        public MusicFileList(in string path) {
            listPath = path;

            var xroot = XDocument.Load(path).Root;
            if (xroot?.Name.LocalName is not "musicFileList" ) {
                throw new InvalidOperationException("musicFileList element is not found.");
            }

            albums = Album.Load(xroot).ToList();
            musicFiles = MusicFile.Load(xroot).ToList();
        }

        public void SaveMusicFileList(in string? path = null) {
            var xdoc = XDocument.Load(path ?? listPath);
            
            var xroot = xdoc.Root;
            if ( (xroot is null) || (xroot.Name.LocalName is not "musicFileList") ) {
                throw new InvalidOperationException($"音楽ファイルリスト {path ?? listPath} に要素 musicFileList がありません。");
            }
            var xnamespace = xroot.Name.Namespace;
            var xalbums = xroot.Element(xnamespace + "albums");
            if ( xalbums is null ) {
                xalbums = new XElement(xnamespace + "albums");
                xroot.Add(xalbums);
            }
            var xalbumList = xalbums.EnumerableElement(xnamespace + "album");
            foreach ( var album in albums ) {
                if ( !xalbumList.Any(a => (a.StringAttribute("name") == album.Name) && (a.StringAttribute("artist") == album.Artist)) ) {
                    xalbums.Add(new XElement(xnamespace + "album", new XAttribute[] { new XAttribute("name", album.Name), new XAttribute("artist", album.Artist) }));
                }
            }
            var xmusicFiles = xroot.Element(xnamespace + "musicFiles");
            if ( xmusicFiles is null ) {
                xmusicFiles = new XElement(xnamespace + "musicFiles");
                xroot.Add(xmusicFiles);
            }
            var xmusicList = xmusicFiles.EnumerableElement(xnamespace + "musicFile");
            foreach ( var musicFile in musicFiles ) {
                if ( xmusicList.Any(m => (m.StringAttribute("album") == musicFile.Album) && (m.StringAttribute("title") == musicFile.Title)) ) {
                    var xe = xmusicList.Where(m => (m.StringAttribute("album") == musicFile.Album) && (m.StringAttribute("title") == musicFile.Title)).First();
                    var xf = xe.Element(xnamespace + "file");
                    xf?.Attribute("size")?.SetValue(musicFile.FileInfo.Size);
                    xf?.Attribute("sizeUnit")?.SetValue(musicFile.FileInfo.SizeUnit.ToString());
                    xf?.Element(xnamespace + "path")?.SetValue(musicFile.FileInfo.FilePath);
                    var xc = xe.Element(xnamespace + "copiedData");
                    xc?.Attribute("copiedCount")?.SetValue(musicFile.CopiedInfo.CopiedCount);
                    xc?.Attribute("lastCopiedDateTime")?.SetValue(musicFile.CopiedInfo.LastCopiedDateTime.ToString("o"));
                } else {
                    var xe = new XElement(xnamespace + "musicFile", new XAttribute[]{
                        new XAttribute("musicFileType", musicFile.MusicFileType),
                        new XAttribute("title", musicFile.Title),
                        new XAttribute("album", musicFile.Album),
                        new XAttribute("isExclusion", musicFile.IsExclusion ? "yes" : "no")
                    });
                    var xf = new XElement(xnamespace + "file", new XAttribute[] {
                        new XAttribute("size", musicFile.FileInfo.Size.ToString()),
                        new XAttribute("sizeUnit", musicFile.FileInfo.SizeUnit.ToString()),
                    });
                    var xp = new XElement(xnamespace + "path") {
                        Value = musicFile.FileInfo.FilePath
                    };
                    var xc = new XElement(xnamespace + "copiedData", new XAttribute[]{
                        new XAttribute("copiedCount", musicFile.CopiedInfo.CopiedCount),
                        new XAttribute("lastCopiedDateTime", musicFile.CopiedInfo.LastCopiedDateTime.ToString("o")),
                    });
                    xf.Add(xp);
                    xe.Add(xf);
                    xe.Add(xc);
                    xmusicFiles.Add(xe);
                }
            }
            xdoc.Save(path ?? listPath);
        }

        public void UpdateMusicFile(in (Album Album, MusicFile MusicFile) musicFileInfo) {
            if ( !Albums.Contains(musicFileInfo.Album) ) {
                logger.Info($"音楽ファイル {musicFileInfo.MusicFile.FileInfo.FilePath} を収録しているアルバム {musicFileInfo.Album.Name} を新規に追加します。");
                albums.Add(musicFileInfo.Album);
            }

            if ( !MusicFiles.Contains(musicFileInfo.MusicFile) ) {
                musicFiles.Add(musicFileInfo.MusicFile);
            } else {
                var i = musicFiles.IndexOf(musicFileInfo.MusicFile);
                musicFiles[i].UpdateFileInfo(musicFileInfo.MusicFile.FileInfo);
            }
        }

        private IEnumerable<MusicFile> EnumerateTransferMusicFile(long transferSize) {
            var albums = Albums.Select(a => (a, MusicFiles.Where(mf => (mf.Album == a.Name) && !mf.IsExclusion)
                                                          .Average(mf => mf.CopiedInfo.CopiedCount)))
                               .ToRandom()
                               .OrderBy(a => a.Item2);

            var sizesum = 0L;
            var musicFiles = new List<MusicFile>();
            foreach ( var (album, copiedCount) in albums ) {
                var mfs = MusicFiles.Where(mf => (mf.Album == album.Name) && !mf.IsExclusion);
                var s = mfs.Select(mf => mf.FileInfo.ToByte()).Sum();
                if ( transferSize < (sizesum + s) ) {
                    break;
                }
                sizesum += s;
                MaxTransferSize?.Invoke(this, sizesum);
                foreach ( var mf in mfs ) {
                    musicFiles.Add(mf);
                }
            }

            return musicFiles.ToEnumerable();
        }

        public async Task TransferAsync(string transferDirectoryPath, long transferSize, CancellationToken token) {
            try {
                logger.Info("転送を開始します");
                var transferedSize = 0L;
                foreach ( var mf in EnumerateTransferMusicFile(transferSize) ) {
                    token.ThrowIfCancellationRequested();
                    var trans = $"{mf.Album} - {mf.Title}";
                    logger.Trace($"転送する音楽ファイルは {mf.Album} - {mf.Title} です。");
                    Transfering?.Invoke(this, mf);
                    var fi = new FileInfo(mf.FileInfo.FilePath);
                    if ( File.Exists(fi.FullName) is true ) {
                        var dname = Path.Combine(transferDirectoryPath, fi.Directory?.Name ?? String.Empty);
                        if ( Directory.Exists(dname) is false ) {
                            logger.Info($"転送先フォルダ {dname} が存在しないため、作成します。");
                            _ = Directory.CreateDirectory(dname);
                        }
                        logger.Info("転送先にコピーします。");
                        {
                            using var reader = fi.OpenRead();
                            using var writer = File.Create(Path.Combine(dname, fi.Name));
                            await reader.CopyToAsync(writer, token);
                        }
                        transferedSize += mf.FileInfo.ToByte();
                        Transfered?.Invoke(this, transferedSize);
                        logger.Info("コピーが完了しました。");
                        mf.UpdateCopiedInfo();
                    }
                }
                logger.Info("転送が完了しました。");
            } catch ( TaskCanceledException ) {
                logger.Info("転送が中断されました。");
            } catch ( Exception e ) {
                logger.Error(e, "転送中に例外が発生しました。");
                throw;
            } finally {
            }
        }
    }
}
