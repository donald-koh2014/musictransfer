﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

using NLog;
using MusicTransfer2.Tools;
using System.Diagnostics.CodeAnalysis;

namespace MusicTransfer2.Music {
    public enum MusicFileType {
        [Description("Unknown")]
        Unknown = 0,
        [Description("FLAC")]
        Flac = 1,
        [Description("MPEG-4 Audio")]
        M4a = 2,
    }

    internal class MusicFile : IEqualityComparer<MusicFile> {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public MusicFileType MusicFileType { get; }
        public string Title { get; }
        public string Album { get; }
        public bool IsExclusion { get; }
        public MusicFileInfo FileInfo { get; private set; }
        public CopiedInfo CopiedInfo { get; }
        public MusicFile(in MusicFileType type, in string title, in string album, in bool isExclusion, in MusicFileInfo fileInfo, in CopiedInfo copiedInfo) {
            MusicFileType = type;
            Title = title;
            Album = album;
            IsExclusion = isExclusion;
            FileInfo = fileInfo;
            CopiedInfo = copiedInfo;
        }

        public async static IAsyncEnumerable<string> GetMusicFilesAsync(string path) {
            logger.Trace($"Getting music files at {path} - Async");

            var dirs = Directory.EnumerateDirectories(path);

            foreach ( var dir in dirs ) {
                await foreach ( var file in GetMusicFilesAsync(dir) ) {
                    yield return file;
                }
            }

            foreach ( var file in Directory.EnumerateFiles(path) ) {
                yield return file;
            }
        }

        public static (Album Album, MusicFile MusicFile) Load(string path) {
            using var reader = new BinaryReader(File.OpenRead(path));
            var ext = Path.GetExtension(path).ToLower();
            logger.Trace($"ファイル \"{path}\" からタグを抽出します。拡張子は \"{ext}\" です。");
            var tags = ext switch {
                ".flac" => Flac.LoadTags(reader),
                // ".m4a" => LoadM4aTags(stream),
                _ => throw new InvalidOperationException($"未対応の拡張子 ({ext}) です。"),
            };

            var album = tags.Where(tag => tag.TagName.ToUpper() == "ALBUM").First().TagValue;
            var albumArtist = tags.Where(tag => tag.TagName.ToUpper() == "ALBUMARTIST").First().TagValue;
            var title = tags.Where(tag => tag.TagName.ToUpper() == "TITLE").First().TagValue;
            var fsize = new FileInfo(path).Length / (double)SizeUnit.MB;

            return (new Album(album, albumArtist), new MusicFile(MusicFileType.Flac, title, album, false,
                new MusicFileInfo(fsize, SizeUnit.MB, path), new CopiedInfo(0, DateTime.MinValue)));
        }

        public void UpdateFileInfo(in MusicFileInfo fileInfo) {
            FileInfo = fileInfo;
        }

        public void UpdateCopiedInfo() {
            CopiedInfo.UpdateNow();
        }

        public static IEnumerable<MusicFile> Load(XElement xElement) {
            var ns = xElement.Name.Namespace;
            var elements = xElement.Element(ns + "musicFiles").EnumerableElement(ns + "musicFile");

            foreach (var element in elements) {
                var fileType = element.StringAttribute("musicFileType");

                if (fileType == null) {
                    throw new InvalidOperationException("Music file type attribute is not found.");
                }

                var type = fileType switch {
                    "Flac" => MusicFileType.Flac,
                    "M4a" => MusicFileType.M4a,
                    _ => MusicFileType.Unknown,
                };

                var title = element.StringAttribute("title");
                var album = element.StringAttribute("album", false);
                var isExclusion = element.YesNoAttribute("isExclusion", false, false);
                var fileInfo = MusicFileInfo.Load(element);
                var copiedInfo = CopiedInfo.Load(element);

                yield return new MusicFile(type, title, album, isExclusion, fileInfo, copiedInfo);
            }
        }

        public override bool Equals(object? obj) {
            return obj is MusicFile file &&
                   MusicFileType == file.MusicFileType &&
                   Title == file.Title &&
                   Album == file.Album;
        }

        public override int GetHashCode() {
            return HashCode.Combine(MusicFileType, Title, Album);
        }

        public bool Equals(MusicFile? x, MusicFile? y) {
            return x is not null &&
                y is not null &&
                x.Equals(y);
        }

        public int GetHashCode([DisallowNull] MusicFile obj) {
            return HashCode.Combine(obj.MusicFileType, obj.Title, obj.Album);
        }

        public static bool operator ==(MusicFile? left, MusicFile? right) {
            return EqualityComparer<MusicFile>.Default.Equals(left, right);
        }

        public static bool operator !=(MusicFile? left, MusicFile? right) {
            return !(left == right);
        }
    }

    internal class MusicFileInfo {
        private const string elementName = "file";
        public double Size { get; }
        public SizeUnit SizeUnit { get; }
        public string FilePath { get; }
        public MusicFileInfo(in double size, in SizeUnit sizeUnit, in string filePath) {
            Size = size;
            SizeUnit = sizeUnit;
            FilePath = filePath;
        }

        public static MusicFileInfo Load(XElement xElement) {
            var ns = xElement.Name.Namespace;
            var fileElement = xElement.Element(ns + elementName);

            if ( fileElement is null ) {
                throw new InvalidOperationException($"要素 {xElement.Name} に必要な要素 {elementName} がありません。");
            }

            var size = fileElement.ParsableAttribute<double>("size", true);
            var unit = fileElement.StringAttribute("sizeUnit", false) switch {
                "MB" => SizeUnit.MB,
                "GB" => SizeUnit.GB,
                "TB" => SizeUnit.TB,
                _ => throw new InvalidOperationException("ファイルサイズの単位指定に誤りがあります。"),
            };
            var path = fileElement.StringElement(ns + "path");

            return new MusicFileInfo(size, unit, path);
        }
    }

    internal class CopiedInfo {
        private const string elementName = "copiedData";
        public int CopiedCount { get; private set; }
        public DateTime LastCopiedDateTime { get; private set; }
        public CopiedInfo(in int copiedCount, in DateTime lastCopied) {
            CopiedCount = copiedCount;
            LastCopiedDateTime = lastCopied;
        }

        public void UpdateNow() {
            CopiedCount++;
            LastCopiedDateTime = DateTime.Now;
        }

        public static CopiedInfo Load(XElement xElement) {
            var ns = xElement.Name.Namespace;
            var element = xElement.Element(ns + elementName);

            if ( element is null ) {
                throw new InvalidOperationException($"要素 {xElement.Name} に必要な要素 {elementName} がありません。");
            }

            var count = element.ParsableAttribute("copiedCount", false, 0);
            var lastCopied = element.ParsableAttribute("lastCopiedDateTime", false, DateTime.MinValue);

            return new CopiedInfo(count, lastCopied);
        }
    }
}
