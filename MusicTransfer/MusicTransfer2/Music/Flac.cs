﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MusicTransfer2.Tools;

using NLog;

namespace MusicTransfer2.Music {
    public enum BlockType {
        StreamInfo = 0,
        Padding = 1,
        Application = 2,
        SeekTable = 3,
        VorbisComment = 4,
        CueSheet = 5,
        Picture = 6,
        Invalid = 127,
    }

    internal static class Flac {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();
        private class MetadataBlockHeader {
            public BlockType BlockType { get; }
            public bool IsLastBlock { get; }
            public int MetadataLength { get; }

            public MetadataBlockHeader(in bool isLastBlock, in BlockType blockType, in int metadataLength) {
                IsLastBlock = isLastBlock;
                BlockType = blockType;
                MetadataLength = metadataLength;
            }

            public static MetadataBlockHeader Load(in BinaryReader reader) {
                var b = reader.ReadByte();
                var isLastBlock = (b & 0x8) == 0x8;
                var blockType = (BlockType)(b & 0x7);
                byte[] bigEndian = new byte[4];
                reader.ReadBytes(3).CopyTo(bigEndian, 1);
                var length = BitConverter.ToInt32(bigEndian.ChangeEndian().ToArray(), 0);
                return new MetadataBlockHeader(isLastBlock, blockType, length);
            }
        }

        public static (string TagName, string TagValue)[] LoadTags(BinaryReader reader) {
            if ( !IsFlacFile(reader) ) {
                var msg = "FLAC ファイルの先頭ブロックが \"fLaC\" ではありません。";
                logger.Error(msg);
                throw new InvalidOperationException(msg);
            }

            var tags = Array.Empty<(string TagName, string TagValue)>();

            bool eol = false;
            while ( !eol ) {
                var header = MetadataBlockHeader.Load(reader);
                eol = header.IsLastBlock;
                if ( header.BlockType == BlockType.VorbisComment ) {
                    // VendorString を読み飛ばす
                    _ = reader.BaseStream.Seek(reader.ReadInt32(), SeekOrigin.Current);
                    // Tag 読み取り
                    var imax = reader.ReadInt32();
                    tags = new (string TagName, string TagValue)[imax];
                    for ( var i = 0; i < imax; i++ ) {
                        var len = reader.ReadInt32();
                        var word = Encoding.UTF8.GetString(reader.ReadBytes(len)).Split(new[] { '=' }, 2);
                        tags[i].TagName = word[0];
                        tags[i].TagValue = word[1];
                    }
                } else {
                    // ブロックデータをスキップする
                    reader.BaseStream.Position += header.MetadataLength;
                }
            };

            return tags;
        }

        private static bool IsFlacFile(in BinaryReader reader) {
            return reader.ReadChars(4).SequenceEqual(new[] { 'f', 'L', 'a', 'C' });
        }
    }
}
