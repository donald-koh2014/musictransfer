﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;

using Reactive.Bindings;
using Reactive.Bindings.Extensions;

namespace MusicTransfer2 {
    internal class MainWindowModelView : INotifyPropertyChanged, IDisposable {
#pragma warning disable CS0067 // メモリリーク予防のために実装している。s
        public event PropertyChangedEventHandler? PropertyChanged;
#pragma warning restore CS0067

        private CompositeDisposable Disposable { get; } = new CompositeDisposable();
        public ReactiveProperty<bool> IsSynchronizing { get; } = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> IsTransfering { get; } = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> IsBusy { get; } = new ReactiveProperty<bool>(false);

        public ReactiveProperty<int> AlbumCount { get; } = new ReactiveProperty<int>();
        public ReactiveProperty<int> TrackCount { get; } = new ReactiveProperty<int>();
        public ReactiveProperty<long> TransferSize { get; } = new ReactiveProperty<long>();
        public ReactiveProperty<long> TransferedSize { get; } = new ReactiveProperty<long>();
        public ReactiveProperty<string> TransferSizeUnit { get; } = new ReactiveProperty<string>();
        public ReactiveProperty<string> TransferingMusicFile { get; } = new ReactiveProperty<string>();

        private void UpdateIsBusy() {
            IsBusy.Value = IsSynchronizing.Value | IsTransfering.Value;
        }

        public void SetIsSynchronizing(in bool isSynchronizing) {
            IsSynchronizing.Value = isSynchronizing;
            UpdateIsBusy();
        }

        public void SetIsTransfering(in bool isTransfering) {
            IsTransfering.Value = isTransfering;
            UpdateIsBusy();
        }

        public MainWindowModelView() {
            _ = IsSynchronizing.AddTo(Disposable);
            _ = IsTransfering.AddTo(Disposable);
            _ = AlbumCount.AddTo(Disposable);
            _ = TrackCount.AddTo(Disposable);
            _ = TransferSize.AddTo(Disposable);
            _ = TransferedSize.AddTo(Disposable);
            _ = TransferSizeUnit.AddTo(Disposable);
            _ = TransferingMusicFile.AddTo(Disposable);
        }

        public void Dispose() {
            Disposable.Dispose();
        }
    }
}
