﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows.Markup;

namespace MusicTransfer2 {

    internal class EnumSourceProvider<T> : MarkupExtension {
        private static string DisplayName([DisallowNull] T value) {
            return (value.GetType().GetField(value.ToString()
                ?? String.Empty)?.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault() as DescriptionAttribute)?.Description ?? String.Empty;
        }

        public static IEnumerable Source =>
            typeof(T).GetEnumValues().Cast<T>()
            .Select(value => new { Code = value, Name = value is null ? String.Empty : DisplayName(value) });

        public override object ProvideValue(IServiceProvider serviceProvider) {
            return this;
        }
    }

    public enum SizeUnit : long {
        [Description("MB")]
        MB = 1000000,
        [Description("GB")]
        GB = 1000000000,
        [Description("TB")]
        TB = 1000000000000,
    }

    internal class SizeUnitSourceProvider : EnumSourceProvider<SizeUnit> { }
}